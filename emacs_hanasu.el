;; 音声合成をするプロセスを走らせる
(defvar *ojtalk* (start-process "ojtalk" "*ojtalk*" "~/ojtalk.sh"))
(setq  *ojtalk* (start-process "ojtalk" "*ojtalk*" "~/ojtalk.sh"))
(delete-process *ojtalk*)
(defvar *py1* (start-process "py_ehanasu" "*pye*" "/usr/bin/python2.7" "/home/takaaki/test1.py"))
(setq *py1* (start-process "py_ehanasu" "*pye*" "/usr/bin/python2.7" "/home/takaaki/test1.py"))
(process-send-eof *py1*)
;; 音声合成のプロセスにUnixソケットでつなげる
(defvar *cn1* (make-network-process :name "cn1" :buffer "*cn1*" :family 'local :remote "/tmp/example.sock"))
(defvar *cn1* *py1*)


;; adviceを加えて動きに応えて、何かを話すようにする
(defun next-line-ad-test1 (&optional x d)
  (process-send-string "py_ehanasu" (format "%dぎょうめ\n" (line-number-at-pos))) 'after)
(advice-add 'next-line :after 'next-line-ad-test1)
(advice-add 'previous-line :after 'next-line-ad-test1)

;; addがtならadvice関数を加える、nilなら除く
(defun control-advice-func (add)
  (loop
   for f in
   '(next-line previous-line)
   do
   (eval
    (if add
	`(advice-add f :after 'next-line-ad-test1)
      `(advice-remove f 'next-line-ad-test1)))))

(advice-remove 'next-line 'next-line-ad-test1)
(advice-remove 'previous-line 'next-line-ad-test1)
